\section{\ac{SEA} syntax and semantics}
\label{sec:runn-example-backgr}

\begin{figure*}[tb]
  % \centering
  \subfloat[A DDD-mode pacemaker controlling human cardiac conduction
  rhythm  \label{fig:heartpacemakera}]
  {\includegraphics[scale=0.4]{./figures/heartpacemaker}}
  \qquad
  \subfloat[The RAA \ac{SEA} in the heart
  model\label{heartpacemakerb}]{\includegraphics[scale=0.45]{./figures/RAA}}
  \qquad
  \subfloat[RAA \ac{SEA}
  trace\label{heartpacemakerc}]{\includegraphics[scale=0.6]{./figures/RAAtrace}}
  \caption{The heart model and the DDD-mode pacemaker}
  \label{fig:heartpacemaker}
\end{figure*}

We use the pacemaker from~\cite{chen14} controlling the human cardiac
rhythm as an example, shown in Figure~\ref{fig:heartpacemaker}, to
describe the syntax and semantics of a network of \acp{SEA}.

% The formal description of the syntax and semantics is provided
% in~\cite{DBLP:journals/tecs/MalikRATM17}.

Figure~\ref{fig:heartpacemakera} consists of two main components: the
heart model (the plant) and the pacemaker (the controller). The heart
model is a network of \acp{SEA} communicating with each other. The
pacemaker is a network of \acp{TA}~\cite{alur94} controlling erratic
heart rhythm. The pacemaker senses two outputs from the heart:
\textcircled{1} the \textit{Atrial Sense} (AS) and \textcircled{2} the
\textit{Ventricular Sense} (VS). In case there is no AS or VS,
indicating the natural heart rhythm, then the pacemaker produces an
\textit{Atrial Pace} (AP) or \textit{Ventricular Pace} (VP),
respectively. The pacemaker model is not our contribution and is
obtained from~\cite{chen14}, hence we refer the reader to~\cite{chen14}
to get details on the working of the pacemaker. We will use the the
heart model to describe the syntax and semantics of \acp{SEA}.

Two auto-rhythmic cells, cells that get excited on their own, SA and SP
model the human atrium in Figure~\ref{fig:heartpacemakera}. The
ventricle on the other hand is modelled using muscle cells, cells that
are driven by external input, RAA and RVA. The delay buffers, in
Figure~\ref{fig:heartpacemakera}, model the propagation delay of the
electrical conduction pathway between the atrium and the ventricle.

The RAA node gets excited by the external input $gv$, at time $t$, given
by
$gv(t) = (v_{1}(t-\Delta) - v(t)) + (v_{2}(t-\Delta) - v(t)) + AP(t)$, where
$\Delta$ is the time delay between nodes SA/SP and RAA, $v(t)$ is the RAA
node' own voltage at time $t$. The behaviour of the RAA node is
implemented as a \ac{SEA}, shown in Figure~\ref{heartpacemakerb} and its
output trace is shown in Figure~\ref{heartpacemakerc}. There are five
locations ($q_{0}$, $q_{1}$, $q_{2}$, $q_{3}$, and $q_{4}$) in the RAA
\ac{SEA}. Location $q_{0}$ is the initial location marked with an
incoming arrow. Initially the voltage of RAA ($v(0)$) is 0. In location
$q_{0}$, the voltage of RAA does not increase (shown by the \ac{ODE}
$\dot{v}=0$). The output variable $AS$ captures the current voltage of
RAA in all locations via the equation $AS = v$. The external voltage
$gv$ excites the RAA shown by the guard $gv \geq V_{t}$, where $V_{t}$ is
some threshold voltage on the edge connecting locations $q_{0}$ and
$q_{1}$. Upon satisfaction of this guard, the \ac{SEA} makes a jump from
$q_{0}$ to $q_{1}$. In location $q_{1}$ the voltage of RAA starts
increasing until it reaches the threshold ($V_{t}$), shown by the
invariant on the location $v \leq V_{t}$. As soon as the RAA voltage
crosses $V_{t}$, a jump is made to location $q_{2}$, where the voltage
increases faster still until it reaches the overshoot voltage ($V_{o}$).
Upon reaching the overshoot voltage, the RAA node now goes into the
so-called refractory period, modelled using location $q_{3}$. Once out
of the refractory period, crossing $V_{r}$, the \ac{SEA} model continues
to repolarise to zero volts (in location $q_{4}$). This process
continues forever. Notice that the internal voltage and the output
voltage ($AS$) are updated using the relations ($v' = v$ and $AS = v$)
on each edge of the \ac{SEA}. Different erratic heart behaviours can be
modelled by changing the parameters and threshold voltages in the
\ac{SEA} to verify the behaviour of the pacemaker.

\begin{definition}
\label{def:1}
Formally, a \acf{SEA} is
$\mathcal{S} = \langle Q, X, V, Y, Init, f, h, Inv, E, G, R, \delta\rangle$, where:
  \begin{itemize}
    \setlength\itemsep{1pt}
  \item $\mathbf{Q}$ a set of discrete locations.
  \item $X$ is a finite collection of continuous variables, with its
    domain represented as $\mathbf{X} = \mathbb{R}^{\|\mathbf{X}\|}$.
  \item $V$ is a finite collection of input variables. We assume
    $V = V_{D} \cup V_{C}$, where $V_{D}$ are discrete inputs and
    $V_{C}$ are continuous inputs, with their respective domains
    $\mathbf{V}_{C}$, $\mathbf{V}_{D}$, and $\mathbf{V}$, respectively.
  \item $Y$ is a finite collection of output variables. We assume that
    $Y = Y_{D} \cup Y_{C}$, where $Y_{D}$ is a collection of discrete
    output variables and $Y_{C}$ is a collection of continuous output
    variables, with their respective domains $\mathbf{Y}_{C}$,
    $\mathbf{Y}_{D}$, and $\mathbf{Y}$, respectively.
  \item $Init \subseteq \{q_0\} \times x_{0} \times y_{0}$ such that there is a singleton
    initial location $q_0 \in \mathbf{Q}$, where $x_{0} \in \mathbf{X}$ and
    $y_{0} \in \mathbf{Y}$.
  \item
    $f : \mathbf{Q} \times \mathbf{X} \times \mathbf{V}\rightarrow
    \mathbb{R}^{\|\mathbf{X}\|}$ is a vector field. Function
    $f(q, x, v)$ is globally \textit{Lipschitz} continuous in
    $x \in \mathbf{X}$ and continuous in $v \in \mathbf{V}$.
  \item $h : \mathbf{Q} \times \mathbf{X} \rightarrow \mathbf{Y}$ is a
    vector field. Function $h(q, x)$ is globally \textit{Lipschitz}
    (left or right) continuous in $x \in \mathbf{X}$.
  \item $Inv: \mathbf{Q} \rightarrow 2^{\mathbf{X} \times \mathbf{V}}$ assigns
    to each $q \in \mathbf{Q}$ an invariant set.
  \item $E \subset \mathbf{Q} \times \mathbf{Q} $ is a collection of discrete edges.
  \item $G : E \rightarrow 2^{\mathbf{X} \times \mathbf{V}}$ assigns to
    each $e = (q, q') \in E$ a guard.
  \item
    $R : E \times \mathbf{X} \times \mathbf{V} \rightarrow 2^{\mathbf{X}
      \times \mathbf{Y}}$ assigns to each $e = (q,q') \in E$,
    $x \in \mathbf{X}$, $v \in \mathbf{V}$ a reset relation.
  \item $\delta$: is the duration of any discrete execution step so that
    the elapsed time at any time step $k \in \mathbb{N}$ can be computed
    as $k \times \delta$.
  \end{itemize}
  \label{def:sea}
\end{definition}

Relating the formal definition (Definition~\ref{def:sea}) of \ac{SEA} to
the \ac{SEA} in Figure~\ref{heartpacemakerb}, we have the set of
locations $\mathbf{Q} = \{q_{0}, q_{1}, q_{2}, q_{3}, q_{4}\}$. The set
of internal variables $X = \{v\}$ with the domain $\mathbb{R}$. The set
of continuous inputs $V_{C} = \{gv\}$ and set of discrete inputs
$V_{D} = \emptyset$, hence the set of total inputs is $V = \{gv\}$, with the
domain $\mathbb{R}$. Similarly the output set
$Y = Y_{C} \cup Y_{D} = \{AS\}$, with the domain $\mathbb{R}$. The initial
condition associated with location $q_{0}$ is
$Init = \{q_{0}\} \times \{0\}$. The \acp{ODE} evolving the continuous
variables are vector fields $f$. For example, the \ac{ODE} in location
$q_{1}$ is described formally as
$f(q_1, v, gv){\overset {\underset {\mathrm {def} }{}}{=}}\ [\dot{v}] =
[sd_{1}]$. Similarly the vector field $h$ assigns values to outputs. For
example, in location $q_{1}$ we have, formally,
$h(q_{1}, v) {\overset {\underset {\mathrm {def} }{}}{=}}\ [AS] = [v]$.
Example of invariants on the location and the edge guards would be:
$Inv(q_1)=\{v|v \leq V_{t}, v \in \mathbf{X}\}$ and
$G(q_1, q_2)=\{v|v \geq V_{t}, v \in \mathbf{X}\}$ for location $q_{1}$ and
edge $(q_{1}, q_{2}) \in E$. When a given edge is taken, the final value
of the variables are reset using the relation $R$, e.g.,
$R(q_1, q_2, v, gv) {\overset {\underset {\mathrm {def} }{}}{=}}\ v' :=
v, AS := v$, where $v'$ is the updated value of $v$.


\begin{definition}
  \label{def:guard}
  Given a set of variables $ T = \{X \cup V\}$, then a set of
  constraints over these variables are defined as follows:\newline
  $g::=t<q | t \leq q| t > q | t \geq q | g \wedge g$ where,
  $q \in \mathbb{Q}$ and $t \in T$. $CV(T)$ denotes the set of
  constraints over $T$.
  \label{def:cvx} 
\end{definition}

\begin{definition}
  \label{def:well-formed}
  We consider a \ac{SEA} to be \textit{well-formed} \textrm{iff} the
  following conditions hold:
  \begin{enumerate}
    \setlength\itemsep{0pt}
  \item The vector field $f$ forms an \ac{LTI} system.
  \item The witness function $x_j(t)$, for $x_j \in X$ obtained after
    solving the ODE, in any location is monotonic and horizontally not
    asymptotic, except in the case of constant functions, when control
    remains in that location, and
  \item The invariants and edge guards adhere to
    Definition~\ref{def:cvx}.
  \end{enumerate}
\end{definition}

We introduce \textit{well-formedness} criteria for \ac{SEA} in
Definition~\ref{def:well-formed}. These criteria guarantee
\textcircled{1} sound and decidable verification of a network of
\acp{SEA} and, \textcircled{2} the turning points of the trajectory,
e.g., when voltage of RAA reaches $V_{t}$, $V_{o}$, etc
(cf. Figure~\ref{fig:heartpacemaker}) can be correctly captured without
the need for dynamic level crossing detection.

\subsection{Semantics}
\label{sec:semantics}

We start by giving the informal description of the semantics, followed
by formalising them.

\begin{figure*}[tb]
  \centering
  \subfloat[Synchronous execution semantics of a network of
  \acp{SEA}\label{fig:semanticsa}]
  {\includegraphics[scale=0.55]{./figures/semantics}}
  \qquad
  \subfloat[Communication between a network of
  \acp{SEA} via input/output registers \label{fig:swioamod}]{
    \scalebox{0.3}{\input{./figures/swiomodular.latex}} }
  \qquad
  \subfloat[Execution trace of a network of \acp{SEA}. Dashed line
  indicates suspension of execution of
  \acp{SEA}. \label{fig:swioaexec}]{
    \scalebox{0.5}{\input{./figures/swioaexec_new.latex}} }
  \caption{Synchronous execution semantics of \ac{SEA}.}
  \label{fig:semantics}
\end{figure*}

\begin{figure}[b]
  \centering
  \includegraphics[scale=0.65]{./figures/RAAtracediscrete}
  \caption{The discrete trace generated by the synchronous execution of
    RAA}
  \label{fig:semanticsb}
\end{figure}

% Unlike standard \ac{HA}/\ac{HIOA} the semantics of \ac{SEA} is a finite
% state space system. In this section we give the informal semantics of
% \ac{SEA} using Figure~\ref{fig:semantics}. The formal semantics of
% \ac{SEA} are provided in~\cite{DBLP:journals/tecs/MalikRATM17}.


\subsubsection{Single \ac{SEA} semantics}
\label{sec:singleseasemantics}

Figure~\ref{fig:semanticsa} shows the \textit{time-set} semantics of two
\acp{SEA} --- RAA and LRI. % We first describe the semantics of RAA the
% first square box in Figure~\ref{fig:semanticsa}.
A time-set ($I_{i}^{k_{i}}$) represents the time that the \ac{SEA}
remains in a location. Each time-set consists of $k_{i}$ discrete
time-steps, each time-step of size $\delta$. Referring to
Figure~\ref{fig:semanticsa}, the square brackets $[\ldots]$, represent
time-sets. Each tick ($|$) in the time-set represents a time-step.
Hence, the time-step labelled $I_{1}^{5}$ is the fifth discrete
time-step in the very first time-set, while $I_{3}^{3}$ is the third
time-step in the third time-set. Each time-step ($k_{i}$) captures one
discrete evolution of the \acp{ODE} in any given location. All
time-steps inside the time-sets, evolving the \acp{ODE}, are formally
called \textit{intra-location} transitions. Since each \ac{SEA} evolves
the \acp{ODE} in discrete time-steps, the output trace of the \ac{SEA}
is discrete. The output trace of the RAA \ac{SEA} following the discrete
time-step semantics (Figure~\ref{fig:semanticsa}) is shown in
Figure~\ref{fig:semanticsb}. When the outgoing edge guard, from any
location, is satisfied, the \ac{SEA} takes the outgoing edge. These
outgoing edges are called \textit{inter-location} transitions. Each
inter-location transition takes one time-step\footnote{Unlike
  \ac{HA}/\ac{HIOA} where edge transitions are instantaneous.} and
indicates the end of the current time-set and start of the next
time-set. Special attention needs to be paid to inter-location
transitions whose edge guards are closed intervals. It is possible that
for a large time-step size $\delta$, an edge guard is incorrectly missed. For
example, consider some continuous variable $x$ in some location. If its
initial value is $x(0) = 1$, its rate of change is given by the \ac{ODE}
$\dot{x} = x$, and the outgoing edge guard is $x == 10$, then with
$\delta = 100$ the value of $x$ will jump to $101$ in a single time-step. In
such cases, as soon as the continuous variable $x$ crosses the edge
guard, \ac{SEA} \textit{saturates} the value of $x$ to 100 (in this
case), thereby forcefully satisfying the edge guard. Saturation only
occurs just before inter-location transitions as indicated by the Gray
boxes in Figure~\ref{fig:semanticsa}.

Definitions~\ref{def:timeset} and~\ref{def:execution} formalise the
execution semantics of a \ac{SEA}.

\begin{definition}
  \label{def:timeset}
  A discrete hybrid timeset is a sequence of intervals
  $\tau = \{I_0^{k_0}, % I_1^{k_1},
  \ldots, I_{i}^{k_{i}}, \ldots, I_n^{k_n} \}$, such that:

    \begin{enumerate}
      \setlength\itemsep{0pt}
    \item
      $I_i^{k_i} = [ \tau_i=\tau_i^0, \tau_i^1=\tau_i + \delta, ...,
      \tau_i'=\tau_i + k_i \times \delta]$
    \item If $n < \infty$ then
      $I_n^{k_n} = [ \tau_n=\tau_n^0, \tau_n^1=\tau_n + \delta, ...,
      \tau_n'=\tau_n + k_n \times \delta]$, or
      $I_n^{k_n} = [ \tau_n=\tau_n^0, \tau_n^1=\tau_n + \delta, ...,
      \tau_n'=\tau_n + k_n \times \delta)$, and
    \item $\tau_i \leq \tau_i'$ and $\tau_i' + \delta = \tau_{i+1}$ for all
      $i < n$.
    \end{enumerate}
\end{definition}

\begin{remark}
  Given $\tau = \{I_0^{k_0}, I_1^{k_1}, ..., I_n^{k_n} \}$
  \begin{enumerate}
    \setlength\itemsep{0pt}
  \item {\color{black} We denote {\boldmath$\tau$} as the domain of $\tau$, which 
      includes any time instant $t$ that is in any interval $I_i^{k_i} \in \tau$.}
  \item Each interval $I_i^{k_i}$ has $k_i$ sub-intervals where
    $k_i=\frac{\tau_i'-\tau_i}{\delta}$, and
  \item Any two consecutive intervals, $I_i^{k_i}$ and $I_{i+1}^{k_{i+1}}$
    are separated by $\delta$ time units.
  \item The non-instantaneous $\delta$ separation between the time
    intervals $I_i^{k_i}$ and $I_{i+1}^{k_i+1}$ disables instantaneous
    mode switches. This in turn disallows infinite state changes in
    finite time, thereby avoiding Zeno artifacts that plague other hybrid
    system models.
  \end{enumerate}
\end{remark}

\begin{definition}
  \label{def:execution}
  Let $\Gamma$ be a collection of discrete hybrid timesets.
  A discrete execution of an \ac{SEA} $\mathcal S$ is a five tuple
  ${\mathcal X}=(\tau, q, x, v , y)$ with $\tau \in \Gamma$,
  {\color{black}
    $q:$ {\boldmath$\tau$} $\rightarrow \mathbf{Q}$,
    $x:$  {\boldmath$\tau$} $\rightarrow \mathbf{X}$,
    $v:$  {\boldmath$\tau$} $\rightarrow \mathbf{V}$, and
    $y:$  {\boldmath$\tau$} $\rightarrow\mathbf{Y}$} satisfying the following:

  \begin{enumerate}
    \setlength\itemsep{0pt}
  \item Initial Condition: $(q(\tau_0), x(\tau_0), y (\tau_{0})) \in Init$
  \item Intra location transitions: For all $i: \tau_i < \tau_i'$, $q, x, v, y$ are 
    continuous over $[\tau_i, \tau_i']$ the following must hold:
    \begin{itemize}
    \item  for all $t \in  [\tau_i,  \tau_i')$, $\frac{d}{dt} x(t) = f(q(t), x(t), v(t))$
      % \{ \tau_i, \tau_i+ \delta, ..., \tau_i+k_i \times \delta \}$, $\frac{d}{dt} x(t) = f(q(t), x(t), v(t))$.
    \item for all $t \in \{ \tau_i, \tau_i+ \delta, ..., \tau_i+(k_i-1) \times
      \delta \}$ $(x(t), v(t)) \in Inv(q(t))$, and
    \item Saturation: at $t=\tau_i'$ $x(t) = saturate(q(t), x(\tau_{i}'))$ % and $(x(t), v(t)) \in Inv(q(t))$
      .
    \end{itemize}
  \item Inter-location transitions: For all $i$, $e_i = (q(\tau_i'),
    q(\tau_{i+1})) \in E$, $( x(\tau_i'), v(\tau_i')) \in
    G(e_{i})$ \\
    and $(x(\tau_{i+1}), y(\tau_{i+1})) \in R(e_i, x(\tau_i'), v(\tau_i'))$, and
  \item For all $t \in \{ \tau_i, \tau_i+ \delta, ..., \tau_i+k_i \times \delta \}$, $y(t) = h(q(t), x(t))$.
  \end{enumerate}
\end{definition}


\subsubsection{Semantics of a network of \acp{SEA}}
\label{sec:networkseasemantics}

A network of \acp{SEA} communicate using input/output variables.
Figure~\ref{fig:swioamod} shows the overview of communication between
\acp{SEA}. All communication happens via input and output registers.
Each \ac{SEA} first reads the input variables from the input register.
Performs its local time-step, and then waits for all other \acp{SEA} to
complete their individual time-steps --- essentially barrier
synchronizing with them. Once all \acp{SEA} have completed their
individual time-steps (intra-location or inter-location) they write
their outputs to the output register, which immediately updates the
inputs registers and the \acp{SEA} can then proceed to read the inputs
for the next time-step and so an and so forth. The overview of the
synchronous execution semantics is shown in
Figure~\ref{fig:swioaexec}.

Figure~\ref{fig:swioaexec} shows the execution of three \acp{SEA}, each
executing their local time-steps ($k_{i}$). Each time-step is at maximum
$\delta$ units in size. If any \ac{SEA} finishes execution before
$\delta$ time units, it waits for other \acp{SEA} to finish execution. Thus,
overall \acp{SEA} execute synchronously using barrier synchronisation
(shown using the Global time-steps in Figure~\ref{fig:swioaexec}). These
execution semantics allow interleaving of inter-location and
intra-location transitions. Hence, given individual execution of two
\acp{SEA} $S_{1}$ and $S_{2}$, their overall execution semantics can be
described using the following rules:

\begin{enumerate}
  \setlength\itemsep{0pt}
\item \SHArule{Inter-Inter}: This happens when $S_{1}$ and $S_{2}$ both
  take inter-location transitions using the same $\delta$ step. Then the
  composite system also takes an inter-location transition of $\delta$ step.
\item \SHArule{Inter-Intra}: \ac{SEA} $S_{1}$ can take an inter-location
  transition of $\delta$ step, while $S_{2}$ takes an intra-location
  transition of $\delta$ step. In this case, the composite system takes an
  intra-location transition of $\delta$ step.
\item \SHArule{Intra-Inter}: \ac{SEA} $S_{2}$ can take an inter-location
  transition of $\delta$ step, while $S_{1}$ takes an intra-location
  transition of $\delta$ step. In this case, the composite system takes an
  intra-location transition of $\delta$ step.
\item \SHArule{Intra-Intra}: Either of the \acp{SEA} can take an
  intra-location transition of $\delta$ step simultaneously. In this case,
  the composite system takes an intra-location transition of $\delta$ step.
\end{enumerate}

The result of the application of all the above rules is shown in
Figure~\ref{fig:semanticsa} with RAA and LRI executing synchronously.

% Figure 3: Give three SEAs (2 via delay buffer and one with the
% pacemaker) and their connection and describe the syntax (relating it to
% the formal syntax). Give the restrictions on the SEA. Give the overall
% semantics with discrete steps figure and describe how they work,
% exchange data, etc.

% Example we will use to describe the HA and the properties we want to
% verify: Train-gate.

% Give the formal semantics and syntax of the \ac{HIOA} here.

% Properties we will verify:

% \begin{itemize}
% \item Safety property: the train is not at position 0 and the gate is
%   down!
% \item The gate goes up and down infinitely often.
% \end{itemize}






%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../verification"
%%% End:
