\section{Compilation  to Promela}
\label{sec:comp-verif}

\newsavebox{\promelacode}
\begin{lrbox}{\promelacode}
  \begin{scriptsize}
\begin{minipage}{0.7\linewidth}
\begin{Verbatim}[numbers=left, commandchars=\\\[\]]
q1:
 if ::(c_expr{(v >= Vt)})-> \label[ln:interstart]
 /*Taking outgoing inter-location
 transition*/
 d_step { /*d_step: atomic step*/
   c_code {AS = v; v_u = v ; };
   raa_tick = 1;
 };\label[ln:interend]
 :: else -> 
 /*Intra-location transition*/
 if :: (c_expr {(v <= Vt)} -> \label[ln:intrastart]
 d_step { /*Atomic step*/ \label[ln:intradstep]
   c_code { \label[ln:intraccodestart]
     /*Evolving v, via Forward Euler*/
     v_u = (sd1 * d) + v ;
     /*Saturation*/\label[ln:sat]
     v_u = (v_u > Vt && !signbit(sd1)) ? Vt : v_u; \label[ln:signbit]
     AS_u = v_u ; /*Setting output*/
     /*Input value into delay
     buffer and get the delayed value (v),
     which is transmitted to node RVA*/
     v = sa_get_and_insert(v_u);
   }; \label[ln:intraccodeend]
   raa_tick = 1;
 }; /*d_step ends*/
   fi; \label[ln:intraend]
  fi;
\end{Verbatim}
\end{minipage}
\end{scriptsize}
\end{lrbox}

\begin{figure}[tb]
  \centering

  \subfloat[RAA \ac{FSM} translated from the \ac{SEA} in
  Figure~\ref{heartpacemakerb}\label{fig:raafsma}]
  {\includegraphics[scale=0.5]{./figures/RAAFSM}}
  \qquad
  \subfloat[Promela code for location $q_{1}$ in
  Figure~\ref{fig:raafsma}\label{fig:raafsmb}]
  {\usebox{\promelacode}}
  \caption{\ac{FSM} and Promela code for the RAA \ac{SEA}}
  \label{fig:raafsm}
\end{figure}

This section describes Step-\textcircled{2} from
Figure~\ref{fig:overall-methodology}. We use the
SPIN~\cite{Holzmann:2003:SMC:1405716} verification framework for
verifying safety and liveness properties, specified in \ac{LTL}.
Promela~\cite{Holzmann:2003:SMC:1405716} is the specification language
of SPIN. Hence, we compile the network of \acp{SEA} into a network of
Promela processes. Each Promela process is a \ac{FSM}, hence our overall
compilation scheme can be described as follows:

\begin{enumerate}
\item Translate each \ac{SEA} into a Promela \ac{FSM} (process)
  \textit{individually}.

\item Establish communication between individual Promela \acp{FSM} using
  two \textit{sets} of global variables: $\mathbf{x}$ and
  $\mathbf{x}_{u}$, representing the input and output registers shown in
  Figure~\ref{fig:swioamod}, respectively.

\item Build an orchestrator \ac{FSM}, which coordinates the execution of
  the network of \acp{FSM}. The orchestrator \ac{FSM} waits for all
  individual \acp{FSM} to complete their local time-steps. Upon
  completion of the individual time-steps the global time-step is
  produced by the orchestrator to synchronize the execution of the
  Promela processes and update the input register variables
  ($\mathbf{x}$) with the values in the output register variables
  ($\mathbf{x}_{u}$). The orchestrator enforces the behavioural
  execution semantics described in Section~\ref{sec:networkseasemantics}
  and shown in Figure~\ref{fig:swioaexec}.
\end{enumerate}

In the rest of this section we describe the \ac{FSM} translation of a
\ac{SEA} and its Promela code.
 
Each \ac{SEA} is translated into a labelled \ac{FSM}. A \ac{FSM} is a
tuple $\left<S, T\right>$, where $S$ are the states, and
$T \subseteq S \times S$, are the transitions.  Moreover, the
transitions of the \ac{FSM} are labelled as: $\frac{guard}{action}$,
which means if the guard holds, then the actions are performed. The
\ac{FSM} translation of the RAA \ac{SEA} from
Figure~\ref{heartpacemakerb} is shown in Figure~\ref{fig:raafsma}. There
is a straightforward translation of every \ac{SEA} into an equivalent
\ac{FSM} thanks to the semantics in Section~\ref{sec:semantics}:

\begin{enumerate}

\item The number of states in the generated \ac{FSM} is exactly the same
  as the number of locations in the original \ac{SEA}.

\item Each intra-location transition is translated into a
  self-transition, evolving the continuous variables in the location
  until the invariant holds. For example, transition $(q_{1}, q_{1})$ is
  a self-transition in Figure~\ref{fig:raafsma}, which implements the
  intra-location transitions of the \ac{SEA}.

\item Each \ac{ODE} is solved using single step forward Euler
  integration. Each transition of the \ac{FSM} corresponds to one local
  time-step. One integration step is performed in each local time-step.
  For example, consider the labels on the transition ($q_{1}, q_{1}$):
  $ \frac{v \leq V_{t}}{v_{u} := v + \delta \times sd_{1}, AS_{u} := v_{u}, raa\_tick
    := 1} $. This label states that, as long as the guard, corresponding
  to the invariant of location $q_{1}$ holds, the \textit{new} value of
  the RAA voltage at the next local time-step, will be
  $v + \delta \times sd_{1}$, where $sd_{1}$ is the slope of the \ac{ODE} (cf.
  Figure~\ref{heartpacemakerb}). Similarly, the value of the atrial
  sense ($AS$), in the next local time-step is assigned to be the
  current voltage of the RAA node. Finally, the completion of the local
  time-step is indicated to the orchestrator, by setting the variables
  $raa\_tick$ to one. Upon completion of \textit{all} the individual
  \acp{FSM}' transitions, the orchestrator will copy the values from
  variable $v_{u}$ to $v$, and $AS_{u}$ to $AS$, reset the value of
  $raa\_tick$ to zero and relaunch \acp{FSM} again to enable their next
  transition.

\end{enumerate}

The generated Promela code, for location $q_{1}$, in the RAA \ac{FSM} is
shown in Figure~\ref{fig:raafsmb}.
Lines~\ref{ln:interstart}-\ref{ln:interend} show the inter-location
transition ($q_{1}, q_{2}$), while
lines~\ref{ln:intrastart}-\ref{ln:intraend} show the intra-location
(self) transition ($q_{1}, q_{1}$). There are a number of important
points of note:

\begin{enumerate}
\item All guard and assignment expressions, working on continuous
  variables (e.g., line~\ref{ln:interstart}) are enclosed within
  \texttt{c\_expr} or \texttt{c\_code} blocks, respectively. Promela
  does \textbf{not} support floating point computations natively. Hence,
  the generated Promela code contains C-code blocks, which are executed
  outside of SPIN during model-checking. However their (Boolean or
  Integer type) return results are used to drive the \ac{FSM}. For
  example, although the guard expression \mbox{\texttt{v >= Vt}}
  (line~\ref{ln:interstart}), is checked outside of the SPIN
  model-checker, its Boolean result (1/0) is used by the SPIN
  model-checker during verification.

\item Every \ac{FSM} transition in Promela is implemented atomically,
  enclosed within the so called \texttt{d\_step} statement. The
  \texttt{d\_step} statement enforces that no other transition from any
  other process, recall that there are a number of Promela processes
  converted from the network of \acp{SEA}, can be interleaved with the
  execution of the statements of the current transition.

\item Any C-code library/system can be incorporated in our verification
  framework. For example, saturation (cf.
  Section~\ref{sec:singleseasemantics}) calls the \texttt{signbit}
  function (line~\ref{ln:signbit}) on the slope of the \ac{ODE} to check
  if the slope is increasing or decreasing. Incorporating any C-code
  allows us to verify the final executable rather than just a model.

\end{enumerate}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../verification"
%%% End:
