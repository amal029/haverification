\section{Introduction}
\label{sec:introduction}

\acf{CPS}~\cite{alur2015principles} are applications that control
complex physical phenomenon (usually called the plant) using digital
controllers. Many \ac{CPS} systems are safety critical, e.g., cardiac
pacemakers~\cite{zhihao12} where a single failure can lead to
catastrophic damage. Formal modelling languages and formal verification
techniques are employed to mitigate risks when designing safety critical
\ac{CPS} systems.

\ac{HA}~\cite{alur93} is a well known formal programming language for
design of safety critical \ac{CPS}. A \ac{HA} uses a combination of
discrete \emph{locations} or \textit{modes} and \acp{ODE} to capture the
continuous dynamics of an individual mode. \ac{HA} was developed for
simulating complex plant models adjoined with their respective
controllers. However, \ac{HA} does not provide a means to specify and
model many interacting plant and controller components.
\ac{HIOA}~\cite{lynch03} was developed as an \ac{HA} extension for
specifying and simulating complex \ac{CPS} systems with many interacting
components. Recently \ac{HIOA} models have started being used for
developing complex biological phenomenon, especially human cardiac
conduction systems for formal verification of safety critical pacemaker
software~\cite{chen14,zhihao12, DBLP:journals/tbe/AiPRMAYAT18}.

Formal verification of \ac{HIOA} models is non-trivial, because of the
well known negative result that reachability is undecidable for
\ac{HIOA}~\cite{henzinger1995s}. Yet, all hope is not lost, and a
plethora of techniques have been developed for formal verification of a
subset of \ac{HIOA} models~\cite{henzinger1997hytech,
  cimatti2014verifying,
  fan2016automatic,tiwari2015time,DBLP:journals/corr/GaoKCC14,SpaceEx}.
All these frameworks target ``safety property'' verification on a subset
of \ac{HIOA}, i.e., guaranteeing that nothing bad ever happens. There is
no known technique to verify ``liveness properties'' of \ac{HIOA}, i.e.,
something (good) eventually happens. Liveness properties are essential
for guaranteeing correct behaviour of medical devices such as
pacemakers. For example, a simple \ac{LTL} liveness property
guaranteeing that the pacemaker beats the human atrium and the ventricle
alternately cannot currently be formally verified for \ac{HIOA} models
of the human cardiac conduction system. There are many more complex
guarantees that can only be specified as liveness properties. However,
to the best of our knowledge, there exists no known \ac{HIOA}
verification framework that support verification of liveness properties.

Our main \textbf{contribution} in this work is a modular verification
framework for a subset of \ac{HIOA} called
\acf{SEA}~\cite{DBLP:journals/tecs/MalikRATM17}, which allows scalable
\textit{safety} \textit{and} \textit{liveness} property verification of
complex hybrid systems. The formal semantics of the network of \acp{SEA}
play an essential role in modular compilation, and soundness of
verification.

\begin{figure}[tb]
  \centering
  \includegraphics[scale=0.4]{./figures/overall}
  \caption{Overall methodology for modularly compiling and verifying
    safety and liveness properties of a network of \acp{SEA}.}
  \label{fig:overall-methodology}
\end{figure}

The overall methodology of this work is presented in
Figure~\ref{fig:overall-methodology}. There are four steps:
Step-\textcircled{1}: designing the plant model as \acp{SEA} and
adjoining it with the controller. Step-\textcircled{2}: compiling
\acp{SEA} and the controller \textit{individually} into Promela
processes. Step-\textcircled{3}: verifying the Promela processes against
the specified safety and liveness \ac{LTL} properties, using the SPIN
model-checker~\cite{Holzmann:2003:SMC:1405716}. If the verification
fails, then the plant or the controller model can be refined
(Step-\textcircled{4} (a)), else we have successfully verified the
design (Step-\textcircled{4} (b)).

The rest of the paper describes each of the steps in
Figure~\ref{fig:overall-methodology}. We start with qualitatively
comparing our contribution with the current state-of-the-art in
Section~\ref{sec:related-work}. Section~\ref{sec:runn-example-backgr}
then uses the verification of the pacemaker adjoined with the human
cardiac conduction system as the running example to describe \ac{SEA}
syntax and semantics (Step-\textcircled{1}).
Section~\ref{sec:comp-verif} describes the compilation of a network of
\ac{SEA} into Promela processes (Step-\textcircled{2}).
Section~\ref{sec:experimental-results} quantitatively compares the
presented verification approach with the current state-of-the-art
(Steps-\textcircled{3} and~\textcircled{4}). Finally we conclude the
paper in Section~\ref{sec:conclusion}.

% The current approach to validation of controllers suffer from the
% following problem

% \begin{enumerate}
% \item Co-simulation: freewheeling, race conditions.
% \item Discrete plant models: cannot capture ODEs, only suitable for
%   closed form solutions,
% \end{enumerate}

% What we want:
% \begin{enumerate}

% \item A language for formal modeling of the plant that can correctly
%   capture plant behavior.
  
% \item A verification approach to controllers.

% \item Synchronous composition of the plant and the controller.

% \end{enumerate}

% The proposed approach:

% \begin{enumerate}
% \item Modeling the plant as a \ac{HIOA}.
% \item Modeling the controller as a FSM.
% \item Synchronously composing the two together.
% \item Translating the composition into a single FSM.
% \item Verifying safety and liveness properties on the generated FSM.
% \end{enumerate}

% The advantages of the proposed approach:
% \begin{enumerate}
% \item The reachability problem is decidable.
% \item We can do liveness property verification.
% \item The overall approach is scalable.
% \end{enumerate}







%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../verification"
%%% End:
