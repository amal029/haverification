#!/usr/bin/env python3

import matplotlib.pyplot as plt
import os
import pandas as pd


def main(args):
    for f in args:
        df = pd.read_csv('./'+f+'.csv')
        df = df.dropna()
        labels = df.axes[1].values
        ax = None
        if f == 'res1':
            ax = df[labels[1:]].plot(kind='bar', legend=False)
            bars = ax.patches
            hatches = ''.join(h*len(df) for h in '\*+')

            for bar, hatch in zip(bars, hatches):
                bar.set_hatch(hatch)

                ax.set_xlabel('Benchmarks')
                ax.set_ylabel('Safety property verification runtime (sec)')
                ax.minorticks_on()
                ax.set_xticklabels(df[labels[0]], rotation='horizontal')
                p, ll = ax.get_legend_handles_labels()
                ax.legend(p, ll, loc='best')

                # The N/A for spaceex
                ax.text(0.00001, 3.5, "N/A", rotation='vertical',
                        size='x-small')
                ax.text(0.9501, 3.5, "N/A", rotation='vertical',
                        size='x-small')
                ax.text(2.9501, 3.5, "N/A", rotation='vertical',
                        size='x-small')

        elif f == 'res2':
            ax = df[labels[1:]].plot(kind='bar', legend=False)
            bars = ax.patches
            hatches = ''.join(h*len(df) for h in '*')

            for bar, hatch in zip(bars, hatches):
                bar.set_hatch(hatch)

                ax.set_xlabel('Benchmarks')
                ax.set_ylabel('Liveness property verification runtime (sec)')
                ax.minorticks_on()
                ax.set_xticklabels(df[labels[0]], rotation='vertical')
                p, ll = ax.get_legend_handles_labels()
                ax.legend(p, ll, loc='best')

        else:
            raise RuntimeError('Unknown file!')

        ax.autoscale(enable=True, tight=True)
        fig = ax.get_figure()
        fig.savefig(os.getenv('HOME')+'/haverification/figures/'+f+'.pdf',
                    bbox_inches='tight')
        plt.close(fig)
        # plt.show()


if __name__ == '__main__':
    plt.style.use('ggplot')
    main(['res1', 'res2'])
